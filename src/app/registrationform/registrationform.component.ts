import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-registration',
  templateUrl: './registrationform.component.html',
  styleUrls: ['./registrationform.component.css']
})
export class RegistrationComponent implements OnInit {
  name:string="";
  email:string="";
  password:string="";
  cpassword:string="";
  gender:string="";
  technologies:any=[];
  languages:any=[];
  empd:any=[];
  constructor() { }

  ngOnInit(): void {
  }
registration(){
    let reqdata = {name:this.name,email:this.email,password:this.password,gender:this.gender,technology:this.technologies,languages:this.languages}
  // console.log(reqdata);

  this.empd.push(reqdata)

}
genderfun(val:any){
  console.log(val.target.value);
  this.gender = val.target.value
}
technologyfun(val:any){
  
  if(val.target.checked){
    this.technologies.push(val.target.value)
  }else{
    let aray = this.technologies.filter((v:any) => v !== val.target.value); 
this.technologies = aray

  }
   console.log("Selected Technolgies",this.technologies)
}
language(val:any){
  if(val.target.checked){
    this.languages.push(val.target.value)
  }else{
    let lan = this.languages.filter((v:any) => v !== val.target.value); 
this.languages = lan

  }
  console.log("Selected Languages",this.languages)
}
  
}






// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-registrationform',
//   templateUrl: './registrationform.component.html',
//   styleUrls: ['./registrationform.component.css']
// })
// export class RegistrationformComponent implements OnInit {
// id:number | undefined;
// name: string = "";
// gender:string="";
// csalary:number | undefined;
// tec:any=["nodejs","angular","database","java"];
// empd:any=[{name:"ram",id:101,cs:2000,tec:"ang",g:"male"}]
//   constructor() { }
//   ngOnInit(): void {
//   }
// save(){
// let newobj = {name:this.name,id:this.id,cs:this.csalary,tec:"node",g:this.gender}
// this.empd.push(newobj)

  
// }
//  select(){
//   this.empd.push(this.gender)
// //   genderfun(val:any){
// //     console.log(val.target.value);
// //     this.gender = val.target.value
// }
// }
