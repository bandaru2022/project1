import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Story1Component } from './story1/story1.component';
import { RegistrationComponent } from './registrationform/registrationform.component';

const routes: Routes = [
  {
    path:"story1",component:Story1Component
  },
  { 
    path:"registrationform",component:RegistrationComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
