import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-story1',
  templateUrl: './story1.component.html',
  styleUrls: ['./story1.component.css']
})
export class Story1Component implements OnInit {
empname:string="";
change:string="";
cchange:string="";
isover:boolean=true;
list:any=[];
empd:any=[{name:"ram",id:101,cs:2000,tec:"ang",g:"male"},
{name:"laxmi",id:102,cs:4000,tec:"ang",g:"female"},
{name:"rani",id:103,cs:1000,tec:"nodejs",g:"female"},
{name:"raju",id:104,cs:6000,tec:"ang",g:"male"},
{name:"mary",id:105,cs:3000,tec:"ang",g:"female"},
{name:"naidu",id:106,cs:8000,tec:"nodejs",g:"male"},
{name:"babu",id:107,cs:5000,tec:"ang",g:"male"},
{name:"robert",id:108,cs:9000,tec:"nodejs",g:"male"},
{name:"jessy",id:109,cs:7000,tec:"ang",g:"female"}]
  constructor() { }

  ngOnInit(): void {
  }
emp(){
  this.empname="sam";
}
rivise(){
  this.change="push";
}
over(){
this.cchange="blue";
}
sdir(){
  this.isover=false;
}

}
